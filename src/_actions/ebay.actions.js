import { ebayConstants } from '../_constants';
import { ebayService } from '../_services';

export const ebayActions = {
    getEbayData,
};

function getEbayData() {
    return dispatch => {
        dispatch(request());

        ebayService.getEbayData()
            .then(
                ebayData => dispatch(success(ebayData)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: ebayConstants.EBAY_DATA_REQUEST } }
    function success(ebayData) { return { type: ebayConstants.EBAY_DATA_SUCCESS, ebayData } }
    function failure(error) { return { type: ebayConstants.EBAY_DATA_FAILURE, error } }
}