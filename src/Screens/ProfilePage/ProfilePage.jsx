import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { userActions } from '../../_actions';

function ProfilePage() {
    const userInit = useSelector(state => state.authentication.user);
    const [user, setUser] = useState({
        id: userInit.id,
        firstName: userInit.firstName,
        lastName: userInit.lastName,
        username: userInit.username,
        street: userInit.address.street,
        town: userInit.address.town,
        county: userInit.address.county,
        postcode: userInit.address.postcode,
        mobile: userInit.mobile,
        company: userInit.company,
        username: userInit.username,
        contact1: userInit.preferences.contact[0],
        contact2: userInit.preferences.contact[1],
        password: ''
    });
    const [submitted, setSubmitted] = useState(false);
    const updating = useSelector(state => state.users.updating);
    const dispatch = useDispatch();

    function handleChange(e) {
        const { name, value } = e.target;
        setUser(user => ({ ...user, [name]: value }));        
    }

    function handleSubmit(e) {
        e.preventDefault();

        setSubmitted(true);
        if (user.firstName && user.lastName && user.username && user.password && user.company && user.street && user.town && user.county && user.postcode && user.mobile && user.contact1 && user.contact2) {
            dispatch(userActions.update(user));
        }        
    }

    return (
        <div className="col-lg-8 offset-lg-2">
            <h2>My Profile</h2>
            <form name="form" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label>First Name</label>
                    <input type="text" name="firstName" value={user.firstName} onChange={handleChange} className={'form-control' + (submitted && !user.firstName ? ' is-invalid' : '')} />
                    {submitted && !user.firstName &&
                        <div className="invalid-feedback">First Name is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Last Name</label>
                    <input type="text" name="lastName" value={user.lastName} onChange={handleChange} className={'form-control' + (submitted && !user.lastName ? ' is-invalid' : '')} />
                    {submitted && !user.lastName &&
                        <div className="invalid-feedback">Last Name is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Street</label>
                    <input type="text" name="street" value={user.street} onChange={handleChange} className={'form-control' + (submitted && !user.street ? ' is-invalid' : '')} />
                    {submitted && !user.street &&
                        <div className="invalid-feedback">Street is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Town</label>
                    <input type="text" name="town" value={user.town} onChange={handleChange} className={'form-control' + (submitted && !user.town ? ' is-invalid' : '')} />
                    {submitted && !user.town &&
                        <div className="invalid-feedback">Town is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>County</label>
                    <input type="text" name="county" value={user.county} onChange={handleChange} className={'form-control' + (submitted && !user.county ? ' is-invalid' : '')} />
                    {submitted && !user.county &&
                        <div className="invalid-feedback">County is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Postcode</label>
                    <input type="text" name="postcode" value={user.postcode} onChange={handleChange} className={'form-control' + (submitted && !user.postcode ? ' is-invalid' : '')} />
                    {submitted && !user.postcode &&
                        <div className="invalid-feedback">Postcode is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Mobile</label>
                    <input type="number" name="mobile" value={user.mobile} onChange={handleChange} className={'form-control' + (submitted && !user.mobile ? ' is-invalid' : '')} />
                    {submitted && !user.lastName &&
                        <div className="invalid-feedback">Mobile is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Username(Email)</label>
                    <input type="username" name="username" value={user.username} onChange={handleChange} className={'form-control' + (submitted && !user.username ? ' is-invalid' : '')} />
                    {submitted && !user.username &&
                        <div className="invalid-feedback">Username(Email) is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Company</label>
                    <input type="text" name="company" value={user.company} onChange={handleChange} className={'form-control' + (submitted && !user.company ? ' is-invalid' : '')} />
                    {submitted && !user.company &&
                        <div className="invalid-feedback">Company is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>1st contact preferences</label>
                    <input type="text" name="contact1" value={user.contact1} onChange={handleChange} className={'form-control' + (submitted && !user.contact1 ? ' is-invalid' : '')} />
                    {submitted && !user.contact1 &&
                        <div className="invalid-feedback">1st contact preferences is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>2nd contact preferences</label>
                    <input type="text" name="contact2" value={user.contact2} onChange={handleChange} className={'form-control' + (submitted && !user.contact2 ? ' is-invalid' : '')} />
                    {submitted && !user.contact2 &&
                        <div className="invalid-feedback">2nd contact preferences is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" name="password" value={user.password} onChange={handleChange} className={'form-control' + (submitted && !user.password ? ' is-invalid' : '')} />
                    {submitted && !user.password &&
                        <div className="invalid-feedback">Password is required</div>
                    }
                </div>
                <div className="form-group">
                    <button className="btn btn-primary">
                        {updating && <span className="spinner-border spinner-border-sm mr-1"></span>}
                        Save
                    </button>
                    <Link to="/" className="btn btn-link">Cancel</Link>
                </div>
            </form>
        </div>
    );
}

export { ProfilePage };