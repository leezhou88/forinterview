import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { userActions } from '../../_actions';

function HomePage() {
    const users = useSelector(state => state.users);    
    const user = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();
    //Inspect user obj here you will find the JSON structure
    useEffect(() => {
        dispatch(userActions.getAll());
    }, []);

    function handleDeleteUser(id) {
        dispatch(userActions.delete(id));
    }
    
    return (
        <div className="col-lg-8 offset-lg-2">
            <h1>Hi {user.firstName}!</h1>
            <p>This is for Interview</p>
            <h3>All registered users:</h3>
            {users.loading && <em>Loading users...</em>}
            {users.error && <span className="text-danger">ERROR: {users.error}</span>}
            {users.items &&
                <ul style={{display: "inline"}}>
                    {users.items.map((user, index) =>
                        <li key={user.id}>
                            <div>id: {user.id}</div>
                            <div>first_name: {user.firstName}</div>
                            <div>last_name: {user.lastName}</div>
                            <label>Address:</label>
                            <div>street: {user.street}</div>
                            <div>town: {user.town}</div>
                            <div>county: {user.county}</div>
                            <div>postcode: {user.postcode}</div>
                            <div>mobile: {user.mobile}</div>
                            <div>email: {user.username}</div>
                            <div>company: {user.company}</div>
                            <label>Preferences:</label>
                            <div>contact: {user.contact1}, {user.contact2}</div>
                            {
                                user.deleting ? <em> - Deleting...</em>
                                : user.deleteError ? <span className="text-danger"> - ERROR: {user.deleteError}</span>
                                : <span> - <a onClick={() => handleDeleteUser(user.id)} className="text-primary">Delete</a></span>
                            }
                        </li>
                    )}
                </ul>
            }
            <p>
                <Link to="/login">Logout</Link>
            </p>
        </div>
    );
}


export { HomePage };