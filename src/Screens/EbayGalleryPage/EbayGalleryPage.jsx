import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import './EbayGalleryPage.css';
import { ebayActions } from '../../_actions';

function EbayGalleryPage() {
    const ebayData = useSelector(state => state.ebay.ebayData);    
    const dispatch = useDispatch();
    //Inspect user obj here you will find the JSON structure
    useEffect(() => {
        dispatch(ebayActions.getEbayData());
    }, []);
    console.log(ebayData ? ebayData.images : 'empty yet')
    
    let _2images = []
    let _27images = []
    if(ebayData) {
        _2images = ebayData.images.map((img, index) => {
            return `http://${img.uri}_2.jpg`;
        })

        _27images = ebayData.images.map((img, index) => {
            return `http://${img.uri}_27.jpg`;
        })
    }

    console.log('2 and 27 images')
    console.log(_2images)
    console.log(_27images)
    
    return (
        <div className="col-lg-8 offset-lg-2">
            <h1>Ebay Interview Page</h1>   
            <p>
                <Link to="/login">Logout</Link>
            </p>
        </div>
    );
}


export { EbayGalleryPage };