import React, { useEffect } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../Screens/HomePage';
import { LoginPage } from '../Screens/LoginPage';
import { RegisterPage } from '../Screens/RegisterPage';
import { ProfilePage } from '../Screens/ProfilePage';
import { FakeGalleryPage } from '../Screens/FakeGalleryPage';
import { EbayGalleryPage } from '../Screens/EbayGalleryPage';
import "./App.css";

function App() {
    const alert = useSelector(state => state.alert);
    const dispatch = useDispatch();

    useEffect(() => {
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    }, []);

    return (
        <div className="jumbotron">
            <div className="container">
                <div className="col-md-8 offset-md-2">
                    <Router history={history}>
                        <Switch>
                            <PrivateRoute exact path="/" component={HomePage} />
                            <Route path="/login" component={LoginPage} />
                            <Route path="/register" component={RegisterPage} />
                            <Route path="/profile" component={ProfilePage} />
                            <Route path="/fake" component={FakeGalleryPage} />
                            <Route path="/ebay" component={EbayGalleryPage} />
                            <Redirect from="*" to="/" />
                        </Switch>
                    </Router>
                </div>
                <div className="text-center">
                    <ul className="indexUl">
                        <li className="indexLi">
                            <a href="/">Home Page</a>
                        </li>
                        <li className="indexLi">
                            <a href="/profile">My Profile Page</a>
                        </li>
                        <li className="indexLi">
                            <a href="/fake">To other content page(fake)</a>
                        </li>
                        <li className="indexLi">
                            <a href="https://www.linkedin.com/in/cms-mobile-app-master/" target="_top">Ruoli's Linkedin</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export { App };