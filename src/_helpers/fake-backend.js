// array in local storage for registered users
let users = JSON.parse(localStorage.getItem('users')) || [];
    
export function configureFakeBackend() {
    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        const { method, headers } = opts;
        const body = opts.body && JSON.parse(opts.body);

        return new Promise((resolve, reject) => {
            // wrap in timeout to simulate server api call
            setTimeout(handleRoute, 500);

            function handleRoute() {
                switch (true) {
                    case url.endsWith('/users/authenticate') && method === 'POST':
                        return authenticate();
                    case url.endsWith('/users/register') && method === 'POST':
                        return register();
                    case url.endsWith('/users') && method === 'GET':
                        return getUsers();
                    case url.endsWith('/ebay') && method === 'GET':
                        return getEbayData();
                    case url.match(/\/users\/\d+$/) && method === 'DELETE':
                        return deleteUser();
                    case url.match(/\/users\/\d+$/) && method === 'UPDATE':
                        return updateUser();
                    default:
                        // pass through any requests not handled above
                        return realFetch(url, opts)
                            .then(response => resolve(response))
                            .catch(error => reject(error));
                }
            }

            // route functions

            function authenticate() {
                const { username, password } = body;
                const user = users.find(x => x.username === username && x.password === password);
                if (!user) return error('Username or password is incorrect');                         
                return ok({
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    address: {
                        street: user.street,
                        town: user.town,
                        county: user.county,
                        postcode: user.postcode,
                    },
                    mobile: user.mobile,
                    company: user.company,                    
                    username: user.username,
                    preferences: {
                        contact: [user.contact1, user.contact2]
                    },
                    token: 'fake-jwt-token'
                });
            }

            function register() {
                const user = body;
    
                if (users.find(x => x.username === user.username)) {
                    return error(`Username  ${user.username} is already taken`);
                }
    
                // assign user id and a few other properties then save
                user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
                users.push(user);
                localStorage.setItem('users', JSON.stringify(users));

                return ok();
            }

            function updateUser() {
                const user = body;
                deleteUser()
                users.push(user);
                localStorage.setItem('users', JSON.stringify(users));

                return ok();
            }
    
            function getUsers() {
                if (!isLoggedIn()) return unauthorized();

                return ok(users);
            }

            function getEbayData() {
                if (!isLoggedIn()) return unauthorized();
                const ebayData = {
                    "id": 97045,
                    "sellerId": 56578,
                    "title": "Buick Tageszulassung Nr. 4402029343513687345",
                    "makeId": 4400,
                    "makeKey": "Buick",
                    "modelKey": "Skylark",
                    "images": [{
                        "uri": "c$",
                        "set": "fe4cfedffdffffffff"
                    }, {
                        "uri": "i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$",
                        "set": "fe4cfedffdffffffff"
                    }, {
                        "uri": "i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/GvsAAOxyZzlTcg-L/$",
                        "set": "fe4cfedffdffffffff"
                    }, {
                        "uri": "i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$",
                        "set": "fe4cfedffdffffffff"
                    }, {
                        "uri": "i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/GpoAAOxyZzlTcg-F/$",
                        "set": "fe4cfedffdffffffff"
                    }, {
                        "uri": "i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/GpoAAOxyZzlTcg-F/$",
                        "set": "fe4cfedffdffffffff"
                    }, {
                        "uri": "i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/Z6UAAOxyCTtTcg-L/$",
                        "set": "fe4cfedffdffffffff"
                    }],
                    "price": {
                        "gross": "566 €",
                        "grs": {
                            "amount": 566.00,
                            "currency": "EUR"
                        },
                        "net": "476 €",
                        "nt": {
                            "amount": 475.63,
                            "currency": "EUR"
                        },
                        "type": "FIXED",
                        "vat": 19.00
                    },
                    "vat": "19,00% MwSt.",
                    "contact": {
                        "type": "Händler",
                        "country": "DE",
                        "enumType": "DEALER",
                        "name": "Handy Andy Home Improvement Center",
                        "address1": "Rudower Chaussee 8",
                        "address2": "DE-46236 Bottrop Altstadt",
                        "phones": [{
                            "type": "PHONE1",
                            "number": "+4930123456789200",
                            "uri": "tel:+4930123456789200"
                        }],
                        "latLong": {
                            "lon": 6.88274,
                            "lat": 51.48694
                        },
                        "withMobileSince": "Händler bei mobile.de seit 10.12.2014",
                        "imprint": "This is a default imprint text to ensure a correct test processing.",
                        "canSendCcMail": true
                    },
                    "isNew": false,
                    "isConditionNew": false,
                    "isPreRegistration": true,
                    "category": "Sportwagen/Coupé, Tageszulassung",
                    "description": "AUTOHAUS JUNGE WELTEN GMBH - ÖFFNUNGSZEITEN :  \\MONTAG bis SAMSTAG von  9.00 - 19.00 UhrAusstattung und Zubehör im Fahrzeuginnenraum* Mercedes A 170\\* Automatikgetriebe\\* Klimaanlage\\* Sitzheizung \\* Tempomat\\* ABS, ESP, Airbag\\* elektrische Spiegel\\* elektrische Fenster\\* Sonnenblenden \\* Kopfstützen aktiv vorn\\* Berganfahrhilfe\\* Außentemperaturanzeige\\* Sitzbelegungserkennung\\* Antriebs-Schlupf-Regelung\\* Fahrlichtassistent\\* Multifunktionslenkrad\\* und viele weitere Außstattungsmerkmale\\Ausstattung und Zubehör außen am Fahrzeug* Blinker im Außenspiegel\\*  Einparkhilfevorn und hinten\\* Nebelscheinwerfer\\* Parameterlenkung elektromechanisch\\* Adaptives Bremslicht blinkend\\* Metalliklackierung\\* und viele weitere Ausstattungsmerkmale\\Sonstige Ausstattung und Zubehörmerkmale* sehr gepflegtes Fahrzeug\\* Scheckheftgepflegt\\* Euro 4 - norm\\* grüne Umweltplakette\\* Eine Finanzierung ist in unserem Autohaus immer möglich\\* 50 Monate Garantie in unserem Autohaus auf Wunsch\\Inzahlungnahme oder Sofortankauf Ihres Pkw jederzeit möglich. Rufen Sie uns einfach an!   \\    SCHAUEN SIE SICH BITTE UNSER ANKAUFSVIDEO AN UNTER \\                          www.Junge-Welten-GmbH.de \\\\Ihr Ansprechpartner     Telefon:      030 - 56 58 56 34\\Herr Alexander Barz      Funk:        0176 - 12 32 32 32 \\Eine Finanzierung auch ohne Anzahlung ist über unsere eigene Hausbank jederzeit und gerne in unseren Autohaus möglich. Rufen Sie uns einfach an wir beraten Sie gerne.\\Zwischenverkauf und Irrtümer für dieses Angebot sind ausdrücklich vorbehalten. Die Fahrzeugbeschreibung dient lediglich der allgemeinen Identifizierung des Fahrzeuges und stellt keine Gewährleistung im kaufrechtlichen Sinne dar. Ausschlaggebend sind einzig und allein die Vereinbarungen in der Auftragsbestätigung oder im Kaufvertrag. Den genauen Ausstattungsumfang erhalten Sie von unserem Verkaufspersonal. Bitte kontaktieren Sie uns.",
                    "attributes": [{
                        "label": "Kilometerstand",
                        "tag": "mileage",
                        "value": "179.616 km"
                    }, {
                        "label": "Hubraum",
                        "tag": "cubicCapacity",
                        "value": "1.320 cm³"
                    }, {
                        "label": "Leistung",
                        "tag": "power",
                        "value": "193 kW (262 PS)"
                    }, {
                        "label": "Kraftstoffart",
                        "tag": "fuel",
                        "value": "Erdgas (CNG), E10-geeignet, Plug-in-Hybrid"
                    }, {
                        "label": "Anzahl Sitzplätze",
                        "tag": "numSeats",
                        "value": "2"
                    }, {
                        "label": "Anzahl der Türen",
                        "tag": "doorCount",
                        "value": "6/7"
                    }, {
                        "label": "Getriebe",
                        "tag": "transmission",
                        "value": "Automatik"
                    }, {
                        "label": "Schadstoffklasse",
                        "tag": "emissionClass",
                        "value": "Euro2"
                    }, {
                        "label": "Umweltplakette",
                        "tag": "emissionsSticker",
                        "value": "2 (Rot)"
                    }, {
                        "label": "Erstzulassung",
                        "tag": "firstRegistration",
                        "value": "08/2010"
                    }, {
                        "label": "Klimatisierung",
                        "tag": "climatisation",
                        "value": "Klimaautomatik"
                    }, {
                        "label": "Airbags",
                        "tag": "airbag",
                        "value": "Front-Airbags"
                    }, {
                        "label": "Farbe",
                        "tag": "color",
                        "value": "Schwarz"
                    }, {
                        "label": "Innenausstattung",
                        "value": "Andere, Grau"
                    }],
                    "features": ["Anhängerkupplung", "Behindertengerecht", "Freisprecheinrichtung", "Navigationssystem", "Sportpaket"],
                    "hasDamage": false,
                    "isDamageCase": true,
                    "htmlDescription": "<b>AUTOHAUS JUNGE WELTEN GMBH<br>ÖFFNUNGSZEITEN :  \\MONTAG bis SAMSTAG von  9.00 - 19.00 Uhr</b><hr><b>Ausstattung und Zubehör im Fahrzeuginnenraum</b><hr><hr>* <b>Mercedes A 170</b>\\* <b>Automatikgetriebe</b>\\* <b>Klimaanlage</b>\\* <b>Sitzheizung </b>\\* <b>Tempomat</b>\\* <b>ABS, ESP, Airbag</b>\\* <b>elektrische Spiegel</b>\\* <b>elektrische Fenster</b>\\* <b>Sonnenblenden </b>\\* <b>Kopfstützen aktiv vorn</b>\\* <b>Berganfahrhilfe</b>\\* <b>Außentemperaturanzeige</b>\\* <b>Sitzbelegungserkennung</b>\\* <b>Antriebs-Schlupf-Regelung</b>\\* <b>Fahrlichtassistent</b>\\* <b>Multifunktionslenkrad</b>\\* <b>und viele weitere Außstattungsmerkmale</b>\\<hr><hr><b>Ausstattung und Zubehör außen am Fahrzeug</b><hr><hr>* <b>Blinker im Außenspiegel</b>\\* <b> Einparkhilfevorn und hinten</b>\\* <b>Nebelscheinwerfer</b>\\* <b>Parameterlenkung elektromechanisch</b>\\* <b>Adaptives Bremslicht blinkend</b>\\* <b>Metalliklackierung</b>\\* <b>und viele weitere Ausstattungsmerkmale</b>\\<hr><hr><b>Sonstige Ausstattung und Zubehörmerkmale</b><hr><hr>* <b>sehr gepflegtes Fahrzeug</b>\\* <b>Scheckheftgepflegt</b>\\* <b>Euro 4 - norm</b>\\* <b>grüne Umweltplakette</b>\\* <b>Eine Finanzierung ist in unserem Autohaus immer möglich</b>\\* <b>50 Monate Garantie in unserem Autohaus auf Wunsch</b>\\<hr><hr><b>Inzahlungnahme oder Sofortankauf Ihres Pkw jederzeit möglich. Rufen Sie uns einfach an!   </b>\\<b>    SCHAUEN SIE SICH BITTE UNSER ANKAUFSVIDEO AN UNTER </b>\\<b>                          www.Junge-Welten-GmbH.de</b> \\<hr><hr>\\<b>Ihr Ansprechpartner     Telefon:      030 - 56 58 56 34\\Herr Alexander Barz      Funk:        0176 - 12 32 32 32</b> \\<hr><hr><b>Eine Finanzierung auch ohne Anzahlung ist über unsere eigene Hausbank jederzeit und gerne in unseren Autohaus möglich. </b><b>Rufen Sie uns einfach an wir beraten Sie gerne.</b>\\<b>Zwischenverkauf und Irrtümer für dieses Angebot sind ausdrücklich vorbehalten. Die Fahrzeugbeschreibung dient lediglich der allgemeinen Identifizierung des Fahrzeuges und stellt keine Gewährleistung im kaufrechtlichen Sinne dar. Ausschlaggebend sind einzig und allein die Vereinbarungen in der Auftragsbestätigung oder im Kaufvertrag. Den genauen Ausstattungsumfang erhalten Sie von unserem Verkaufspersonal. Bitte kontaktieren Sie uns.</b>",
                    "segment": "Car",
                    "vc": "Car",
                    "created": 1418206195,
                    "adMobTargeting": {
                        "make": "Buick",
                        "model": "Skylark",
                        "price": "01",
                        "dealer": "01",
                        "channel": "car",
                        "preis": "566",
                        "ma": "179616",
                        "kw": "193",
                        "ez": "2010",
                        "intid": "97045",
                        "advid": "2514"
                    }
                }
                console.log('this has been called')
                return ok(ebayData);
            }
    
            function deleteUser() {
                if (!isLoggedIn()) return unauthorized();
    
                users = users.filter(x => x.id !== idFromUrl());
                localStorage.setItem('users', JSON.stringify(users));
                return ok();
            }

            // helper functions

            function ok(body) {
                resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(body)) });
            }

            function unauthorized() {
                resolve({ status: 401, text: () => Promise.resolve(JSON.stringify({ message: 'Unauthorized' })) });
            }

            function error(message) {
                resolve({ status: 400, text: () => Promise.resolve(JSON.stringify({ message })) });
            }

            function isLoggedIn() {
                return headers['Authorization'] === 'Bearer fake-jwt-token';
            }
    
            function idFromUrl() {
                const urlParts = url.split('/');
                return parseInt(urlParts[urlParts.length - 1]);
            }
        });
    }
}