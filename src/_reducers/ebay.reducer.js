import { ebayConstants } from '../_constants';

export function ebay(state = {}, action) {
  switch (action.type) {
    case ebayConstants.EBAY_DATA_REQUEST:
      return {
        loading: true
      };
    case ebayConstants.EBAY_DATA_SUCCESS:
      return {
        ebayData: action.ebayData
      };
    case ebayConstants.EBAY_DATA_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}